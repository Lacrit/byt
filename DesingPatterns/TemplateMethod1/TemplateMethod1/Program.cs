﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] intarr = { 3, 5, 7, 84, 4, 6 };
            string[] s = { "Bill", "Gates", "James", "Apple", "Net", "Java" };
            IntBubbleSort intbs = new IntBubbleSort(intarr);
            StringBubbleSort strbs = new StringBubbleSort(s);

            strbs.sort();
            intbs.sort();

            Console.Write("Int array sorted:");
            for ( int i = 0; i < intarr.Length; i++ )
            {
                Console.Write(intarr[i] + " ");
            }
            Console.Write("\nString array sorted:");
            for (int i = 0; i < s.Length; i++)
            {
                Console.Write(s[i] + " ");
            }


        }
    }
}
