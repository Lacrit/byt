﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod1
{
    class StringBubbleSort : BubbleSort
    {
        public string[] str { get; private set; }

        public StringBubbleSort(string[] str)
        {
            this.str = str;
        }

        public void sort()
        {
            int length = str.Length;
            base.sort(length);
        }
        protected override bool outOfOrder(int ind)
        {
            return string.Compare(str[ind], str[ind + 1]) < 0;
        } 
        protected override void swap(int ind)
        {
            string tmp = str[ind];
            str[ind] = str[ind + 1];
            str[ind + 1] = tmp;
        }
    }
}
