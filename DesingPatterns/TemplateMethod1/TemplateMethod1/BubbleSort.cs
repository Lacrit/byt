﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod1
{
    abstract class BubbleSort
    {
        protected virtual void sort(int length)
        {
            if (length <= 1) return;
            for (int nextToLast = length - 2; nextToLast >= 0; nextToLast--)
            {
                for (int first = 0; first <= nextToLast; first++)
                {
                    if (outOfOrder(first)) swap(first);
                }
            }
        }

        protected abstract void swap(int ind);
        protected abstract bool outOfOrder(int ind);
    }
}
