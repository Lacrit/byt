﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod1
{
    class IntBubbleSort : BubbleSort
    {
        public int[] arr { get; private set; }

        public IntBubbleSort(int[] arr)
        {
            this.arr = arr;
        }

        public void sort()
        {
            int length = arr.Length;
            base.sort(length);
        }
        protected override bool outOfOrder(int ind)
        {
            return arr[ind] > arr[ind + 1];
        }

        protected override void swap(int ind)
        {
            int tmp = arr[ind];
            arr[ind] = arr[ind + 1];
            arr[ind + 1] = tmp;
        }
    }
}
