﻿using ChainOfResponsibility.Operations;
using System;

namespace ChainOfResponsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            IChain chainCalc1 = new AddNumbers();
            IChain chainCalc2 = new SubtractNumbers();
            IChain chainCalc3 = new MultNumbers();
            IChain chainCalc4 = new DivideNumbers();

            chainCalc1.setNextChain(chainCalc2);
            chainCalc2.setNextChain(chainCalc3);
            chainCalc3.setNextChain(chainCalc4);

            Command request = new Command(16, 4, "/");

            double? res = chainCalc1.calculate(request);
            Console.Write(res == null
                    ? $"Operation {request.operation} is not supported"
                    : $"{request.num1+request.operation+request.num2} = {res} \n");
        }
    }
}
