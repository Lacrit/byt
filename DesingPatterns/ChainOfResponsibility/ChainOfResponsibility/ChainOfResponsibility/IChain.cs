﻿namespace ChainOfResponsibility
{
    internal interface IChain
    {
        void setNextChain(IChain nextChain);
        double? calculate(Command request);
    }
}