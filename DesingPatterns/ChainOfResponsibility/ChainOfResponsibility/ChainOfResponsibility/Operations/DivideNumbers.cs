﻿namespace ChainOfResponsibility.Operations
{
    class DivideNumbers : IChain
    {
        private IChain nextInChain;
        public void setNextChain(IChain nextChain)
        {
            nextInChain = nextChain;
        }

        public double? calculate(Command request) => (request.operation == "/")
            ? request.num1 / request.num2
            : nextInChain.calculate(request);

    }
}
