﻿namespace ChainOfResponsibility
{
    public class Command
    {
        public int num1 { get; private set; }
        public int num2 { get; private set; } 
        public string operation { get; private set; }
        public Command(int num1, int num2, string operation)
        {
            this.num1 = num1;
            this.num2 = num2;
            this.operation = operation;
        }
    }
}