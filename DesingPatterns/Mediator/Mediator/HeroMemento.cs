﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    class HeroMemento
    {
        public int bullets { get; private set; }

        public HeroMemento(int bullets)
        {
            this.bullets = bullets;
        }

    }
}
