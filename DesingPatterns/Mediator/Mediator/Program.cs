﻿using System;

namespace Memento
{
    class Program
    {
        static void Main(string[] args)
        {
            int bullets = 10;
            HeroOriginator hero = new HeroOriginator(bullets);
            CareTaker game = new CareTaker();
            Console.WriteLine("Initial value: {0}", bullets);
            game.history.Push(hero.save());
            hero.shoot();
            hero.shoot(); 
            hero.restore(game.history.Pop());
            hero.shoot(); 
        }
    }
}
