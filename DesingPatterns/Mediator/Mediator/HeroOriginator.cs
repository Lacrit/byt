﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    class HeroOriginator
    {
        private int bullets;
     
        public HeroOriginator(int bullets)
        {
            this.bullets = bullets;
        }

        public HeroMemento save()
        {
            Console.WriteLine("Current state saved.", bullets);
            return new HeroMemento(bullets);
        }
        

        public void restore(HeroMemento m)  
        {
            bullets = m.bullets;
            Console.WriteLine("State restored.", bullets);
        }

        public void shoot()
        {
            if (bullets > 0)
            {
                --bullets;
                Console.WriteLine("Shoot: {0} bullets left.", bullets);
            }
            else
                Console.WriteLine("There are no bullets left.");
        }
    }
}
