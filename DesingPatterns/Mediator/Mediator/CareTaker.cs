﻿using System.Collections.Generic;

namespace Memento
{
    class CareTaker
    {
        public Stack<HeroMemento> history { get; private set; }
        public CareTaker()
        {
            history = new Stack<HeroMemento>();
        }
    }
}
