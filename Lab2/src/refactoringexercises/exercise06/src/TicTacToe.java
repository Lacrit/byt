package refactoringexercises.exercise06.src;

/*
 * 	Bad smells:
 * 		- long method 
 * 		- duplicative code 
 * 		- divergent change
 */

public class TicTacToe {
	public final int MOVES = 9;
	public StringBuffer board;

	public TicTacToe(String s) {
		board = new StringBuffer(s);
	}

	public TicTacToe(String s, int position, char player) {
		board = new StringBuffer(s);
		board.setCharAt(position, player);
	}

	public int suggestMove(char player) {
		int position = suggestWinningMove(player);
		return (position != -1) ? position : makeDefautMove();
	}

	public int suggestWinningMove(char player) {
		for (int i = 0; i < MOVES; i++) {
			if (checkEmptyCell(i)) {
				TicTacToe game = makeMove(i, player);
				if (game.winner() == player)
					return i;
			}
		}
		return -1;
	}

	public int makeDefautMove() { 
		for (int i = 0; i < MOVES; i++) {
			if (checkEmptyCell(i))
				return i;
		}
		return -1;
	}
	
	public TicTacToe makeMove(int i, char player) {
		return new TicTacToe(board.toString(), i, player);
	}

	public char winner() {
		// check for horizontal winner
		for (int i = 0; i < MOVES; i += 3) {
			if (checkHorizontalLine(i))
				return board.charAt(i);
		}

		// check for vertical winner
		for (int i = 0; i < 3; ++i) {
			if (checkVerticalLine(i))
				return board.charAt(i);
		}

		// check for diagonal winner
		return checkDiagonals();

	}

	private boolean checkHorizontalLine(int i) {
		return checkLine(new int[] { i, i + 1, i + 2 });
	}

	private boolean checkVerticalLine(int i) {
		return checkLine(new int[] { i, i + 3, i + 6 });
	}

	private char checkDiagonals() {
		if (checkLine(new int[] { 0, 4, 8 }))
			return board.charAt(0);
		else if (checkLine(new int[] { 2, 4, 6 }))
			return board.charAt(2);
		else // no winner yet
			return '-';
	}

	private boolean checkLine(int... indices) {
		return (!checkEmptyCell(indices[0]) && board.charAt(indices[1]) == board.charAt(indices[0])
				&& board.charAt(indices[2]) == board.charAt(indices[0]));
	}

	private boolean checkEmptyCell(int ind) {
		return (board.charAt(ind) == '-');
	}
}
