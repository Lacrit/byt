package refactoringexercises.exercise05.src;
import java.io.PrintStream;

/*
    Code Smells: Divergent Change
    Refactoring: Created an object
    
*/

public class CsvWriter {
	
	private final PrintStream outputStream;
	
	public CsvWriter() {
		outputStream = System.out;
	}
    public void write(String[][] lines) {
        for (String[] line : lines) writeLine(line);
    }

    private void writeLine(String[] fields) {
        if (fields.length == 0)
            outputStream.println();
        else {
            writeField(fields[0]);

            for (int i = 1; i < fields.length; i++) {
                outputStream.print(",");
                writeField(fields[i]);
            }
            outputStream.println();
        }
    }

    private void writeField(String field) {
        if (field.indexOf(',') != -1 || field.indexOf('\"') != -1)
            writeQuoted(field);
        else
            outputStream.print(field);
    }

    private void writeQuoted(String field) {
        outputStream.print('\"');
        for (int i = 0; i < field.length(); i++) {
            char c = field.charAt(i);
            if (c == '\"')
                outputStream.print("\"\"");
            else
                outputStream.print(c);
        }
        outputStream.print('\"');
    }
}
