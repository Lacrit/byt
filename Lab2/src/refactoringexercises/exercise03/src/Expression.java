package refactoringexercises.exercise03.src;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Expression {
    private static final Map<Character, BiFunction<Integer, Integer, Integer>> operations = new HashMap<>();
	private int left;
	private int right;
	private Character op;

	public Expression(char op, int left, int right) {
		this.op = op;
		this.left = left;
		this.right = right;
		
	    operations.put('+', (x, y) -> x + y);
	    operations.put('-', (x, y) -> x - y);
	    operations.put('*', (x, y) -> x * y);
	    operations.put('/', (x, y) -> x / y);
	}

	int evaluate() { // simple evaluation
		return operations.get(op).apply(left, right);
	}
}
