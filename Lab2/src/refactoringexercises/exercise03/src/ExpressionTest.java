package refactoringexercises.exercise03.src;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionTest {

    @Test
    public void testAddition() {
        Expression e = new Expression('+', 100, -100);
        assertEquals(e.evaluate(), 0);
    }

    @Test
    public void testSubtraction() {
        Expression e = new Expression('-', 100, -100);
        assertEquals(e.evaluate(), 200);
    }

    @Test
    public void testMultiplication() {
        Expression e = new Expression('*', 100, -100);
        assertEquals(e.evaluate(), -10000);
    }

    @Test
    public void testDivision() {
        Expression e = new Expression('/', 100, -100);
        assertEquals(e.evaluate(), -1);
    }

    @Test
    public void testComplexExpression() {
        // 1+2-3*4/5
        Expression e = new Expression('-',
                new Expression('+',1, 2).evaluate(),
                new Expression('/', new Expression('*', 3, 4).evaluate(),5).evaluate());
        assertEquals(e.evaluate(), 1);
    }

}
