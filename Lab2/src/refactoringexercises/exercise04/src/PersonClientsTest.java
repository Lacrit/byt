package refactoringexercises.exercise04.src;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.io.*;

public class PersonClientsTest {

	@Test
	public void testClients() throws IOException {
		Person bobSmith = new Person("Smith", "Bob", null);
		Person jennyJJones = new Person("Jones", "Jenny", "J");

		StringWriter out = new StringWriter();
		Person.print(out, bobSmith);
		assertEquals("Bob Smith", out.toString());

		out = new StringWriter();
		Person.print(out, jennyJJones);
		assertEquals("Jenny J Jones", out.toString());

		assertEquals("Smith, Bob", bobSmith.toString());
		assertEquals("Jones, Jenny J", jennyJJones.toString());

		out = new StringWriter();
		Person.display(out, bobSmith);
		assertEquals("Smith, Bob", out.toString());

		out = new StringWriter();
		Person.display(out, jennyJJones);
		assertEquals("Jones, Jenny J", out.toString());

		assertEquals("Smith, Bob", bobSmith.toString());
		assertEquals("Jones, Jenny J", jennyJJones.toString());
	}
}
