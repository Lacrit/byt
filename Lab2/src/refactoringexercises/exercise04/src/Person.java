package refactoringexercises.exercise04.src;

import java.io.IOException;
import java.io.Writer;


/* 	Bad Smells: 
 * 		- shotgun surgery 
 * 		- feature envy
 * 		- duplicated code
 * 		- specuative generality
 * 	Refactoring: 
 * 		- pull up methods 
 * 		- delete unused method (formatPerson)
 */

public class Person {
	private String last;
	private String first;
	private String middle;

	public Person(String last, String first, String middle) {
		this.last = last;
		this.first = first;
		this.middle = middle;
	}

	public String getLast() {
		return this.last;
	}
	public String getFirst() {
		return this.first;
	}
	public String getMiddle() {
		return this.middle == null ? "" : " " + this.middle;
	}

	public String toString() {
		return this.getLast() + ", " + this.getFirst() + this.getMiddle();
	}

	static void print(Writer out, Person person) throws IOException {
		out.write(person.getFirst());
		out.write(person.getMiddle());
		out.write(" ");
		out.write(person.getLast());
	}

	static void display(Writer out, Person person) throws IOException{
		out.write(person.toString());
	}


}