package refactoringexercises.exercise01.src;

/*
	Bad Smells: 
		-Long method match
	Refactoring: 
		-Extracted to 3 different methods
*/

public class Matcher {
	public Matcher() {
	}

	public boolean match(int[] expected, int[] actual, int clipLimit, int delta) {
		clipLarge(actual, clipLimit);
		if(isDiffLength(actual, expected))
			return false;		
		if(!isLessThanDelta(actual, expected, delta))
			return false;
		return true;
	}
	
	// Check that each entry within expected +/- delta
	boolean isLessThanDelta(int[] actual, int[] expected, int delta) { 
		for (int i = 0; i < actual.length; i++)
			if (Math.abs(expected[i] - actual[i]) > delta)
				return false;
		return true;
	}

	// Check for length differences
	boolean isDiffLength(int[] actual, int[] expected) { 
		if (actual.length != expected.length)
			return true;
		return false;
	}
	
	// Clip "too-large" values
	void clipLarge(int[] actual, int clipLimit) { 
		for (int i = 0; i < actual.length; i++)
			if (actual[i] > clipLimit)
				actual[i] = clipLimit;
	}
}