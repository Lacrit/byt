package refactoringexercises.exercise02.src;

import java.util.*;

/*
	Bad Smells: 
		-Long method load, duplicated code
	Refactoring: 
		-Extract methods
*/

public class Configuration {
	public int interval;
	public int duration;
	public int departure;

	public void load(Properties props) throws ConfigurationException {
		interval = checkProps("interval" , "monitor interval", props);
		duration = checkProps("duration" , "duration" , props);
		departure = checkProps("departure" , "departure offset", props);
	}
	
	int checkProps(String prop, String key, Properties props) throws ConfigurationException{ 
		String valueString = props.getProperty(prop);
		isNotNull(valueString, key);
		int value = Integer.parseInt(valueString);
		isPositive(value, key);
		if(!prop.equals("interval")) isModZero(value, valueString);
		return value;
	}

	void isNotNull(String value, String key) throws ConfigurationException{ 
		if (value == null) throw new ConfigurationException(key);
	}

	void isPositive(int value, String key) throws ConfigurationException{ 
		if (value <= 0) throw new ConfigurationException(key + " > 0");
	}
	
	void isModZero(int value, String key) throws ConfigurationException { 
		if ((value % interval) != 0) throw new ConfigurationException(key + " % interval");
	}
}
