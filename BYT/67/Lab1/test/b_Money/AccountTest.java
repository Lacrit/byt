package b_Money;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AccountTest {
	Currency SEK, DKK;
	Bank Nordea;
	Bank DanskeBank;
	Bank SweBank;
	Account testAccount;
	
	@Before
	public void setUp() throws Exception {
		SEK = new Currency("SEK", 0.15);
		SweBank = new Bank("SweBank", SEK);
		SweBank.openAccount("Alice");
		testAccount = new Account("Hans", SEK);
		testAccount.deposit(new Money(10000000, SEK));

		SweBank.deposit("Alice", new Money(1000000, SEK));
	}
	
	@Test
	public void testAddRemoveTimedPayment() {
		Money money = new Money(5000, SEK);
        testAccount.addTimedPayment("pid1", 5, 5, money, SweBank, "Alice");
        testAccount.removeTimedPayment("pid1");
	}
	
	@Test
	public void testTimedPayment() throws AccountDoesNotExistException {
		Money money = new Money(5000, SEK);
        Integer aliceBalanceBefore = SweBank.getBalance("Alice");
		Integer testBalanceBefore = testAccount.getBalance().getAmount();

		testAccount.addTimedPayment("pid1", 5, 1, money, SweBank, "Alice");
        testAccount.tick();
		testAccount.tick();

		Integer aliceBalanceAfter = SweBank.getBalance("Alice");
		Integer testBalanceAfter = testAccount.getBalance().getAmount();

		assertEquals(testBalanceBefore -5000, testBalanceAfter, 0);
		assertEquals(aliceBalanceBefore +5000, aliceBalanceAfter, 0);
	}

	@Test
	public void testAddWithdraw() {
		Integer balance = testAccount.getBalance().getAmount();
        testAccount.withdraw(new Money(5000, SEK));
        assertEquals(balance - 5000, testAccount.getBalance().getAmount(), 0);
	}
	
	@Test
	public void testGetBalance() { 
		assertEquals(10000000, testAccount.getBalance().getAmount(), 0);
	}
}
