package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BankTest {
	Currency SEK, DKK;
	Bank SweBank, Nordea, DanskeBank;
	
	@Before
	public void setUp() throws Exception {
		DKK = new Currency("DKK", 0.20);
		SEK = new Currency("SEK", 0.15);
		SweBank = new Bank("SweBank", SEK);
		Nordea = new Bank("Nordea", SEK);
		DanskeBank = new Bank("DanskeBank", DKK);
		SweBank.openAccount("Ulrika");
		SweBank.openAccount("Bob");
		Nordea.openAccount("Bob");
		DanskeBank.openAccount("Gertrud");
	}

	@Test
	public void testGetName() { // why testing getter ? ask pietia 
		assertEquals("SweBank", SweBank.getName());
		assertEquals("Nordea", Nordea.getName());
		assertEquals("DanskeBank", DanskeBank.getName());
	}

	@Test
	public void testGetCurrency() { // whhyyyyyyy
		assertEquals(SEK, SweBank.getCurrency());
		assertEquals(SEK, Nordea.getCurrency());
		assertEquals(DKK, DanskeBank.getCurrency());
	}

	@Test
	public void testOpenAccount() throws AccountExistsException, AccountDoesNotExistException {
		Nordea.openAccount("Ludwig");
	}

	@Test
	public void testDeposit() throws AccountDoesNotExistException {
		Nordea.deposit("Bob", new Money(150, DKK));
		assertEquals(200, Nordea.getBalance("Bob"), 0);
	}

	@Test
	public void testWithdraw() throws AccountDoesNotExistException {
		Nordea.withdraw("Bob", new Money(150, DKK));
		assertEquals(-200, Nordea.getBalance("Bob"), 0);
	}
	
	@Test
	public void testGetBalance() throws AccountDoesNotExistException {
		Nordea.deposit("Bob", new Money(10000, DKK));
		Nordea.withdraw("Bob", new Money(1000, DKK));
		assertEquals(12000, Nordea.getBalance("Bob"), 0);
	}
	
	@Test
	public void testTransfer() throws AccountDoesNotExistException {
		SweBank.deposit("Bob", new Money(10000, SEK));
        SweBank.transfer("Bob", Nordea, "Bob", new Money(1000, SEK));
        assertEquals(9000, SweBank.getBalance("Bob"), 0);
		assertEquals(1000, Nordea.getBalance("Bob"), 0);
	}
	
	@Test
	public void testTimedPayment() throws AccountDoesNotExistException {
		Integer sweBankBalanceBefore = SweBank.getBalance("Bob");
		Integer nordeaBalanceBefore = Nordea.getBalance("Bob");

		SweBank.addTimedPayment("Bob", "pid1", 2, 1, new Money(5000, SEK), Nordea, "Bob");
		SweBank.tick();
		//SweBank.tick();

		assertEquals(sweBankBalanceBefore -5000, SweBank.getBalance("Bob"), 0);
		assertEquals(nordeaBalanceBefore +5000, Nordea.getBalance("Bob"), 0);
}
