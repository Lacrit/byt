package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MoneyTest {
	Currency SEK, DKK, NOK, EUR;
	Money SEK100, EUR10, SEK200, EUR20, SEK0, EUR0, SEKn100;
	
	@Before
	public void setUp() throws Exception {
		SEK = new Currency("SEK", 0.15);
		DKK = new Currency("DKK", 0.20);
		EUR = new Currency("EUR", 1.5);
		SEK100 = new Money(10000, SEK);
		EUR10 = new Money(1000, EUR);
		SEK200 = new Money(20000, SEK);
		EUR20 = new Money(2000, EUR);
		SEK0 = new Money(0, SEK);
		EUR0 = new Money(0, EUR);
		SEKn100 = new Money(-10000, SEK);
	}

	@Test
	public void testGetAmount() {
		assertEquals(10000, SEK100.getAmount(), 0);
		assertEquals(1000, EUR10.getAmount(), 0);
		assertEquals(0, EUR0.getAmount(), 0);
		assertEquals(-10000, SEKn100.getAmount(), 0);
	}

	@Test
	public void testGetCurrency() {
		assertEquals(SEK, SEK100.getCurrency());
		assertEquals(EUR, EUR10.getCurrency());
		assertEquals(SEK, SEK200.getCurrency());
		assertEquals(EUR, EUR20.getCurrency());
	}

	@Test
	public void testToString() {
	    assertEquals("0.0 EUR", EUR0.toString());
		assertEquals("-100.0 SEK", SEKn100.toString());
		assertEquals("10.5 EUR", new Money(1050, EUR).toString());
	}

	@Test
	public void testGlobalValue() {
		assertEquals(1500, EUR10.universalValue(), 0);
		assertEquals(3000, SEK200.universalValue(), 0);
		assertEquals(3000, EUR20.universalValue(), 0);
		assertEquals(0, SEK0.universalValue(), 0);
		assertEquals(0, EUR0.universalValue(), 0);
	}

	@Test
	public void testEqualsMoney() {
		assertTrue(SEK100.equals(EUR10));
		assertTrue(SEK200.equals(EUR20));
		assertTrue(SEK0.equals(EUR0));
	}

	@Test
	public void testAdd() {
		assertTrue(new Money(30000, SEK).equals(SEK100.add(SEK200)));
		assertTrue(new Money(20000, SEK).equals(SEK100.add(EUR10)));
		assertTrue(new Money(30000, SEK).equals(SEK200.add(SEK100)));
	}

	@Test
	public void testSub() {
		assertTrue(new Money(0, SEK).equals(SEK100.sub(EUR10)));
		assertTrue(new Money(-10000, SEK).equals(SEK100.sub(SEK200)));
		assertTrue(new Money(-1000, EUR).equals(EUR0.sub(SEK100)));
	}

	@Test
	public void testIsZero() {
		assertTrue(new Money(0, SEK).isZero());
		assertFalse(new Money(100, EUR).isZero());
	}

	@Test
	public void testNegate() {
		assertTrue(new Money(-10000, SEK).equals(SEK100.negate()));
		assertTrue(new Money(-2000, EUR).equals(EUR20.negate()));
	}

	@Test
	public void testCompareTo() {
		assertEquals(-1, SEK100.compareTo(EUR20), 0);
		assertEquals(0, SEK100.compareTo(EUR10), 0);
	}
}
