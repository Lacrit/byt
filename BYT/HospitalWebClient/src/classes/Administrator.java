package classes;

import java.time.LocalDate;

import Exceptions.InvalidBirthDateException;
import Exceptions.InvalidBloodTypeException;
import Exceptions.InvalidNameOrSurnameException;

public class Administrator extends Person {
	
	public enum privilege {
	    CUSTOMER_REGISTRATION,
	    REQUEST_REVIEW,
	    CUSTOMER_REGISTRATION_AND_REQUEST_REVIEW,
	    NONE
	}	
	
	private privilege privilege; 

	public Administrator(String name, String surname, String phoneNum, String bloodType, LocalDate birthdate, privilege privilege)
			throws InvalidNameOrSurnameException, InvalidBirthDateException, InvalidBloodTypeException {
		super(name, surname, phoneNum, bloodType, birthdate);
		setPrivilege(privilege);
	}

	public privilege getPrivilege() {
		return privilege;
	}

	public void setPrivilege(privilege privilege) {
		this.privilege = privilege;
	}
}
