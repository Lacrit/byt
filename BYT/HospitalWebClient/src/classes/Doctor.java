package classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import Exceptions.InvalidBirthDateException;
import Exceptions.InvalidBloodTypeException;
import Exceptions.InvalidNameOrSurnameException;

public class Doctor extends Person {
	
	private HashMap<Integer, Shift> timetable;
	private ArrayList<Specialization> specializations = new ArrayList<>();
	
	public Doctor(String name, String surname, String phoneNum, String bloodType, LocalDate birthdate)
			throws InvalidNameOrSurnameException, InvalidBirthDateException, InvalidBloodTypeException {
		super(name, surname, phoneNum, bloodType, birthdate);
	}
	
	public HashMap<Integer, Shift> getTimetable() {
		return timetable;
	}

	public void setTimetable(HashMap<Integer, Shift> timetable) {
		this.timetable = timetable;
	}

	public ArrayList<Specialization> getSpecializations() {
		return specializations;
	}

	public void setSpecializations(ArrayList<Specialization> specializations) {
		this.specializations = specializations;
	}

	private void Invite(ChatSession chatSession) { 
		if(chatSession.isOpen()) { 
			if (chatSession.getJoinedDoctor() == null) { 
				chatSession.setJoinedDoctor(this);
			}
		}
	}

	private void makeTimetable() { 
		
	}
}
