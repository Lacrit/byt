package classes;

import java.time.LocalDateTime;

public class ChatSession {
	
	private LocalDateTime dateTime; 
	private String content; 
	private int duration; 
	private boolean isOpen;
	private Doctor joinedDoctor = null;
	

	public ChatSession(LocalDateTime dateTime, String content, int duration) {
		setDateTime(dateTime);
		setContent(content); 
		setDuration(duration);
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	public Doctor getJoinedDoctor() {
		return joinedDoctor;
	}

	public void setJoinedDoctor(Doctor joinedDoctor) {
		this.joinedDoctor = joinedDoctor;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public void open() {
		setOpen(true); 		
	}

	public void close() {
		isOpen = false;
		if (!(joinedDoctor == null)) { 
			joinedDoctor = null;
		}
	}

	private void tryOpenSession() {
		if (!isOpen()) {
			open();
		}
	}

}
