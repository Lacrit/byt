package classes;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import Exceptions.PriceLowerThanZeroException;

public class Procedure extends Service {
	
	private String room; 

	public Procedure(LocalDateTime start, BigDecimal price, int duration, String description, String room)
			throws PriceLowerThanZeroException {
		super(start, price, duration, description);
		setRoom(room); 		
	}
	
	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}
	
	private boolean isRoomAvailable() { 
		// for now it always it since we don`t have Room class
		return true;
	}

}
