package classes;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import Exceptions.PriceLowerThanZeroException;

public class Consultation extends Service {
	
	public enum type { 
		ONLINE, 
		ONSITE
	}
	
	private Doctor doctor;
	private type type;
	
	public Consultation(LocalDateTime start, BigDecimal price, int duration, String description, Doctor doctor, type type) throws PriceLowerThanZeroException {
		super(start, price, duration, description);
		setDoctor(doctor);
		setType(type); 
		
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public type getType() {
		return type;
	}

	public void setType(type type) {
		this.type = type;
	}
	

}
