package classes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import Exceptions.InvalidBirthDateException;
import Exceptions.InvalidBloodTypeException;
import Exceptions.InvalidNameOrSurnameException;

public class Patient extends Person {
	
	private String medicalDetails;
	private ArrayList<Payment> payments;
	private ArrayList<Service> services;

	
	public Patient(String name, String surname, String phoneNum, String bloodType, LocalDate birthdate)
			throws InvalidNameOrSurnameException, InvalidBirthDateException, InvalidBloodTypeException {
		super(name, surname, phoneNum, bloodType, birthdate);
	}
	
	public Patient(String name, String surname, String phoneNum, String bloodType, LocalDate birthdate, String medicalDetails)
			throws InvalidNameOrSurnameException, InvalidBirthDateException, InvalidBloodTypeException {
		super(name, surname, phoneNum, bloodType, birthdate);
		this.medicalDetails = medicalDetails;
	}

	public String getMedicalDetails() {
		return medicalDetails;
	}

	public void setMedicalDetails(String medicalDetails) {
		this.medicalDetails = medicalDetails;
	}

	public ArrayList<Payment> getPayments() {
		return payments;
	}

	public void setPayments(ArrayList<Payment> payments) {
		this.payments = payments;
	}

}
