package classes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import Exceptions.PriceLowerThanZeroException;

public class Payment {
	
	public enum payment { 
		CARD,
		CASH
	}
	
	private LocalDate date; 
	private LocalTime time; 
	private BigDecimal amount; // just in case - it`s an expensive clinic!
	private payment type; 

	public Payment(LocalDate date, LocalTime time, BigDecimal amount, payment type) throws PriceLowerThanZeroException {
		setDate(date);
		setTime(time); 
		setAmount(amount); 
		setType(type);
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) throws PriceLowerThanZeroException {	
		if (!amount.abs().equals(amount)) throw new PriceLowerThanZeroException();
		this.amount = amount;		
	}

	public payment getType() {
		return type;
	}

	public void setType(payment type) {
		this.type = type;
	}

	private void printInvoice() { 
		System.out.print("What an invoice, Mark");
	}
	
	private void refund() { 
		System.out.print("Where is my money, Danny?");
	}
}


