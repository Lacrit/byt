package classes;

import Exceptions.InvalidBirthDateException;
import Exceptions.InvalidBloodTypeException;
import Exceptions.InvalidNameOrSurnameException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;

public abstract class Person {
	private String name;
	private String surname; 
	private long phoneNum; 
	private String bloodType; 
	private LocalDate birthdate;
	
	public Person(String name, String surname, String phoneNum, String bloodType, LocalDate birthdate)
            throws InvalidNameOrSurnameException, InvalidBirthDateException, InvalidBloodTypeException {
		setName(name);
		setSurname(surname);
		setPhoneNum(phoneNum);
		setBloodType(bloodType);
		setBirthdate(birthdate);
	}
	
	
	public String getName() {
		return name;
	}
	public long getPhoneNum() {
		return phoneNum;
	}
	public String getBloodType() {
		return bloodType;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setBloodType(String bloodType) throws InvalidBloodTypeException {
		if(!validBloodType(bloodType)) throw new InvalidBloodTypeException();
		this.bloodType = bloodType;
	}

	private boolean validBloodType(String bloodType) {
		return bloodType.length() > 0 && bloodType.length()<4;
	}
	
	public void setPhoneNum(String phoneNum) throws NumberFormatException{
		try{
			this.phoneNum = Long.parseLong(phoneNum);
		}catch (NumberFormatException ex) {
			System.out.println("Phone number is not valid");
		}
	}
	
	public void setBirthdate(LocalDate birthdate) throws InvalidBirthDateException {
		if (birthdate.isAfter(ChronoLocalDate.from(LocalDateTime.now()))) throw new InvalidBirthDateException();
		this.birthdate = birthdate;
	}
	public void setName(String name) throws InvalidNameOrSurnameException {
		if (name.length() < 1) throw new InvalidNameOrSurnameException();
		this.name = name;
	}
	
	public void setSurname(String surname) throws InvalidNameOrSurnameException {
		if (surname.length() < 1) throw new InvalidNameOrSurnameException();
		this.surname = surname;
	}
}
