package classes;

import java.util.ArrayList;

public class Clinic {

	private String address; 
	private String description;
	private ArrayList<Shift> shifts; 
	private ArrayList<Service> services;
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Shift> getShifts() {
		return shifts;
	}

	public void setShifts(ArrayList<Shift> shifts) {
		this.shifts = shifts;
	}

	public ArrayList<Service> getServices() {
		return services;
	}

	public void setServices(ArrayList<Service> services) {
		this.services = services;
	}
	
	public Clinic(String address, String description, ArrayList<Shift> shifts, ArrayList<Service> services) {
		setAddress(address);
		setDescription(description);
		setShifts(shifts);
		setServices(services);
	}

}
