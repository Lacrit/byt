package Exceptions;

public class InvalidBirthDateException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4016102708121089213L;

	public InvalidBirthDateException() {
        super();
    }
}
