package tests;

import Exceptions.PriceLowerThanZeroException;
import classes.Payment;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class PaymentTest {

    private Payment mockPayment;

    @Before
    public void setUp() throws Exception {
        mockPayment = new Payment(LocalDate.now(), LocalTime.now(),new BigDecimal(1000),Payment.payment.CARD);
    }

    @Test
    public void getAmount() {
        setAmount();
    }

    @Test
    public void setAmount() {
        BigDecimal negativeAmount = new BigDecimal(-1000);
        try {
            mockPayment.setAmount(negativeAmount);
        } catch (PriceLowerThanZeroException e) {
            assertTrue(true);
            e.printStackTrace();
        }
        assertFalse(mockPayment.getAmount().equals(negativeAmount));
    }

}