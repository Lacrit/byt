package tests;

import classes.Shift;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.management.InvalidAttributeValueException;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class ShiftTest {

    private Shift dayShift;
    private Shift nightShift;

    @Before
    public void setUp() throws Exception {
        dayShift = new Shift(LocalTime.NOON, LocalTime.NOON.plusHours(8));
        nightShift = new Shift(LocalTime.MIDNIGHT.minusHours(4), LocalTime.MIDNIGHT.plusHours(4));
    }

    @Test
    public void getStart() {
        setStart();
    }

    @Test
    public void setStart() {
        LocalTime newTimeStart = LocalTime.NOON.minusHours(4);
        dayShift.setStart(newTimeStart);
        Assert.assertEquals(newTimeStart, dayShift.getStart());
    }

    @Test
    public void getEnd() {
        setEnd();
    }

    @Test
    public void setEnd() {
        LocalTime newTimeEnd = LocalTime.MIDNIGHT.plusHours(2);
        nightShift.setEnd(newTimeEnd);
        Assert.assertEquals(newTimeEnd, nightShift.getEnd());
    }

}