package tests;

import Exceptions.InvalidBirthDateException;
import Exceptions.InvalidBloodTypeException;
import Exceptions.InvalidNameOrSurnameException;
import classes.ChatSession;
import classes.Doctor;
import classes.Patient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class ChatSessionTest {

    private ChatSession mockSession;
    private Patient mockPatient;
    private Doctor mockDoc;

    @Before
    public void setUp() throws Exception {
        mockSession = new ChatSession(LocalDateTime.now(), "content", 30);
        mockPatient = new Patient("PName", "PSname", "766756756", "O", LocalDate.now());
        mockDoc = new Doctor("DocName", "DocSname", "567567109", "B", LocalDate.now());
    }

    @Test
    public void close() {
        try {
            mockSession.setJoinedDoctor(new Doctor("DocName", "DocSname", "567567109", "B", LocalDate.now()));
            assertNotEquals(mockSession.getJoinedDoctor(), null);
        } catch (InvalidNameOrSurnameException e) {
            e.printStackTrace();
        } catch (InvalidBirthDateException e) {
            e.printStackTrace();
        } catch (InvalidBloodTypeException e) {
            e.printStackTrace();
        }
        mockSession.close();
        assertEquals(mockSession.getJoinedDoctor(), null);
    }

    @Test
    public void tryOpenSession() {
        mockSession.open();
        Assert.assertTrue(mockSession.isOpen());
    }
}