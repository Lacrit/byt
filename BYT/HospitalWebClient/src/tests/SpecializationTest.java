package tests;

import Exceptions.InvalidNameOrSurnameException;
import classes.Specialization;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SpecializationTest {

    private Specialization mockSpec;

    @Before
    public void setUp() throws InvalidNameOrSurnameException {
        mockSpec = new Specialization("Spec1", "Ultimate doc");
    }

    @Test
    public void getName() {
        try {
            mockSpec.setName("Spec2");
        } catch (InvalidNameOrSurnameException e) {
            e.printStackTrace();
        }
        Assert.assertNotEquals("Spec1", mockSpec.getName());
    }

    @Test
    public void setName() {
        try {
            mockSpec.setName("d");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidNameOrSurnameException);
            e.printStackTrace();
        }
    }

    @Test
    public void getDescription() {
        String descr = "gdkjo;erjge";
        mockSpec.setDescription(descr);
        Assert.assertEquals(mockSpec.getDescription(), descr);
    }

    @Test
    public void setDescription() {
        getDescription();
    }

    @Test
    public void listAllSpecializations(){
        Assert.assertTrue(Specialization.listAllSpecializations().size() > 0);
    }

}