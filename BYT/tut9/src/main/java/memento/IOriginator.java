package memento;

public interface IOriginator<T> {

    Memento<T> save();
    void restore(Memento<T> memento);

}
