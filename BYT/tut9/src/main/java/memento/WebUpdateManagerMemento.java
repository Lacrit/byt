package memento;

public class WebUpdateManagerMemento extends Memento<WebUpdateManagerState> {

    public WebUpdateManagerMemento(WebUpdateManagerState state) {
        super(state);
    }

}
