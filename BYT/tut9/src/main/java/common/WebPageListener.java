package common;

import observer.IListener;

public class WebPageListener implements IListener<UpdateData> {

    private static int currID = 0;
    private int ID;

    public WebPageListener() {
        ID = ++currID;
    }

    @Override
    public void registerEvent(UpdateData data) {
        System.out.println(this + ": " + data);
    }

    @Override
    public String toString() {
        return "main.java.common.WebPageListener#" + ID;
    }

}
