package common;
import memento.IOriginator;
import memento.Memento;
import memento.WebUpdateManagerMemento;
import memento.WebUpdateManagerState;
import observer.IListener;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class WebUpdateManager implements Runnable, IOriginator<WebUpdateManagerState> {

    private HashMap<String, LinkedList<IListener<UpdateData>>> listenerMap;
    private HashMap<String, URLConnection> connectionMap;
    private HashMap<String, UpdateData> updateMap;
    private long updateFreq;
    private boolean shouldStop = false;

    public WebUpdateManager(long updateFreqMillis) {
        this.updateFreq = updateFreqMillis;

        listenerMap = new HashMap<>();
        connectionMap = new HashMap<>();
        updateMap = new HashMap<>();
    }


    public UpdateData checkAddressForUpdates(String address) {
        if(connectionMap.containsKey(address)){
            long tmp = connectionMap.get(address).getLastModified();
            if (updateMap.entrySet().size() > 0) {
                //System.out.println(address + " updated last at " +tmp + "(stored " + updateMap.get(address) +")");
                if (tmp > updateMap.get(address).getUpdateTime().getTime()) {
                    UpdateData newData = new UpdateData(new Date(tmp));
                    updateMap.put(address, newData);
                    broadcastUpdate(address);
                    return newData;
                }
            }
        }
        return null;
    }

    public URLConnection connect(String urlToListen) throws IOException {
        URLConnection conn = new URL(urlToListen).openConnection();
        connectionMap.put(urlToListen, conn);
        listenerMap.put(urlToListen, new LinkedList<>());
        updateMap.put(urlToListen, new UpdateData(new Date(connectionMap.get(urlToListen).getLastModified())));
        return conn;
    }


    private void traverseConnections() {
        if (connectionMap.entrySet().size() > 0) {
            for (Map.Entry entry : connectionMap.entrySet()) {
                checkAddressForUpdates((String) entry.getKey());
            }
        }
    }

    // Observer behavior

    public boolean addListener(IListener<UpdateData> newListener, String urlToListen) throws IOException {
        if (!connectionMap.containsKey(urlToListen)) { // connect for the 1st time and subscribe
            connect(urlToListen);
        }
        if(!listenerMap.get(urlToListen).contains(newListener)) {
            listenerMap.get(urlToListen).add(newListener);
            return true; // subscribe
        }
        return false; // already subscribed
    }


/*
    public void removeListener(observer.IListener listener, String urlToListen) {
        LinkedList<observer.IListener<Long>> listeners = listenerMap.get(urlToListen);
        listeners.remove(listener);
        if (listeners.size() == 0) { // no more subscribers
            connectionMap.remove(urlToListen);
            updateMap.remove(urlToListen);
        }
    }
*/

    private void broadcastUpdate(String updateSource) {
        for (IListener<UpdateData> l : listenerMap.get(updateSource)) {
            l.registerEvent(updateMap.get(updateSource));
        }
    }

    // Threading

    public synchronized void tryStop() {
        this.shouldStop = true;
    }

    private synchronized boolean keepRunning() {
        return !this.shouldStop;
    }

    @Override
    public void run() {
        while (keepRunning()) {
            traverseConnections();
            try {
                Thread.sleep(updateFreq);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // memento.Memento behavior

    @Override
    public Memento<WebUpdateManagerState> save() {
        System.out.println("Saving state...");
        return new WebUpdateManagerMemento(new WebUpdateManagerState(this.listenerMap, this.connectionMap, this.updateMap));
    }

    @Override
    public void restore(Memento<WebUpdateManagerState> memento) {
        System.out.println("Restoring state...");
        this.connectionMap = memento.getState().getConnectionMap();
        this.listenerMap = memento.getState().getListenerMap();
        this.updateMap = memento.getState().getUpdateMap();
    }
}

