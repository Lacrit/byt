package common;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {
            WebUpdateManager manager = new WebUpdateManager(5*1000);

            System.out.println("Assigning listeners.");
            WebPageListener listener1 = new WebPageListener();
            WebPageListener listener2 = new WebPageListener();
            WebPageListener listener3 = new WebPageListener();
            manager.addListener(listener1, "http://www.pja.edu.pl/");
            manager.addListener(listener2, "http://www.pja.edu.pl/");

            System.out.println("Launching thread.");
            Thread managerThread = new Thread(manager);
            managerThread.start();


            System.out.println("Waiting for updates.");
            try {
                Thread.sleep(5 * 60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Terminating.");
            manager.tryStop();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
