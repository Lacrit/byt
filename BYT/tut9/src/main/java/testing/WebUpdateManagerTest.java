package testing;

import common.WebPageListener;
import common.WebUpdateManager;
import memento.WebUpdateManagerMemento;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

// WIP
public class WebUpdateManagerTest {

    WebUpdateManager manager;
    WebPageListener listener;
    WebPageListener listener1;
    String urlString1 = "https://www.baeldung.com/mockito-mock-methods";

    @Before
    public void setUp() throws Exception {
        manager = new WebUpdateManager(5000);
        listener = Mockito.mock(WebPageListener.class);
        listener1 = Mockito.mock(WebPageListener.class);
        try {
            manager.addListener(listener, urlString1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkAddressForUpdates() {
        Assert.assertTrue(false);
    }

    @Test
    public void connect() {
        Assert.assertTrue(false);
    }

    @Test
    public void addListener() {
        Assert.assertTrue(false);
    }

    @Test
    public void save() {

        WebUpdateManagerMemento mem1 = (WebUpdateManagerMemento) manager.save();
        Assert.assertTrue(mem1.getState().getConnectionMap().entrySet().size() == 1);
        Assert.assertTrue(mem1.getState().getUpdateMap().entrySet().size() == 1);
        Assert.assertTrue(mem1.getState().getListenerMap().entrySet().size() == 1);

        try {
            manager.addListener(listener1, urlString1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        WebUpdateManagerMemento mem2 = (WebUpdateManagerMemento) manager.save();
        Assert.assertNotEquals(mem1, mem2);
    }

    @Test
    public void restore() {
        WebUpdateManagerMemento mem1 = (WebUpdateManagerMemento) manager.save();
        try {
            manager.addListener(listener1, urlString1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        WebUpdateManagerMemento mem2 = (WebUpdateManagerMemento) manager.save();
        Assert.assertNotEquals(mem1, mem2);
        manager.restore(mem1);
        WebUpdateManagerMemento mem3 = (WebUpdateManagerMemento) manager.save();
        Assert.assertEquals(mem1.getState(), mem3.getState());
    }

}