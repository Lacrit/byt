﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Observer : MonoBehaviour {

    public GameObject observable;
    public float movementSpeed = 1f;
    public Vector3 imitatedRotationEuler;
    public Vector3 observerRotation;
    public Vector3 rotatedFwd;
    public Vector3 rotatedUp;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (observable.transform.hasChanged)
        {
            observerRotation = observable.transform.eulerAngles;
            Matrix3x3 rX = Matrix3x3.Rotation(observerRotation.x, Matrix3x3.AroundAxis.X);
            Matrix3x3 rY = Matrix3x3.Rotation(observerRotation.y, Matrix3x3.AroundAxis.Y);
            Matrix3x3 rZ = Matrix3x3.Rotation(observerRotation.z, Matrix3x3.AroundAxis.Z);

            rotatedFwd = rZ * rY * rX * Vector3.forward;
            rotatedUp = rZ * rY * rX * Vector3.up;

            Debug.Log("Executed at" + Time.deltaTime);
            imitatedRotationEuler = rZ * rY * rX * transform.eulerAngles;

            transform.LookAt(rotatedFwd + transform.position, rotatedUp);
            observable.transform.hasChanged = false;
        }
    }
}
