﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    //Settings
    public Vector3 velocity;
    public float mass = 1f;
    public float gravConstantAndSunMass = 1f;
    public Transform sun;

    //properties
    public Vector3 position { get { return transform.position;  } set { transform.position = value; } }



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (sun)
        {

            position += velocity * Time.deltaTime;

            float distanceFromSun = (position - sun.position).magnitude;
            float forceMagnitutde = gravConstantAndSunMass * mass / distanceFromSun * distanceFromSun;

            Vector3 force = forceMagnitutde * (sun.position - position).normalized;

            Vector3 acceleration = force / mass;
            Vector3 deltaVelocity = acceleration * Time.deltaTime;

            velocity += deltaVelocity;

        }

	}

    private void OnDrawGizmos()
    {
        // Draw projection onto plane
        if (projectionPlane)
        {
            Vector3 distance = position - projectionPlane.position;
            Vector3 normal = projectionPlane.up;
            float displacementDotNormal = Vector3.Dot(distance, normal);
            Vector3 projected = transform.position - displacementDotNormal * normal;

            Gizmos.color = Color.red;
            Gizmos.DrawLine(projected, transform.position);

            Gizmos.color = Color.green;
            Gizmos.DrawLine(projected, Vector3.zero);
        }
    }
}
